package com.koding.thahara.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class ModelUser{
    public String username;
    public String pass;
    public Boolean status;
    public String role;
    public String key;

    public ModelUser() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public ModelUser(String username, String pass,String role, Boolean status) {
        this.username = username;
        this.pass = pass;
        this.role = role;
        this.status = status;
        this.key = key;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
