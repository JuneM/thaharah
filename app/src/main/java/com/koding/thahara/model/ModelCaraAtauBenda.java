package com.koding.thahara.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class ModelCaraAtauBenda implements Serializable {
    String solusi;
    String mazhab;
    String sumber;
    String keterangan;
    String uid;
    String username;
    String key;

    public ModelCaraAtauBenda(){
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public ModelCaraAtauBenda(String solusi, String mazhab, String sumber, String keterangan, String uid, String username) {
        this.solusi = solusi;
        this.mazhab = mazhab;
        this.sumber = sumber;
        this.keterangan = keterangan;
        this.uid = uid;
        this.username = username;
        this.key = key;
    }

    public String getSolusi() {
        return solusi;
    }

    public void setSolusi(String solusi) {
        this.solusi = solusi;
    }

    public String getMazhab() {
        return mazhab;
    }

    public void setMazhab(String mazhab) {
        this.mazhab = mazhab;
    }

    public String getSumber() {
        return sumber;
    }

    public void setSumber(String sumber) {
        this.sumber = sumber;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
