package com.koding.thahara.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class ModelRule implements Serializable {
    String key_kondisi;
    String key_solusi;
    String label_kondisi;
    String label_solusi;
    String uid;
    String username;
    String key;

    public ModelRule(){

    }

    public ModelRule(String key_kondisi, String key_solusi, String label_kondisi, String label_solusi, String uid, String username) {
        this.key_kondisi = key_kondisi;
        this.key_solusi = key_solusi;
        this.label_kondisi = label_kondisi;
        this.label_solusi = label_solusi;
        this.uid = uid;
        this.username = username;
        this.key = key;
    }

    public String getKey_kondisi() {
        return key_kondisi;
    }

    public void setKey_kondisi(String key_kondisi) {
        this.key_kondisi = key_kondisi;
    }

    public String getKey_solusi() {
        return key_solusi;
    }

    public void setKey_solusi(String key_solusi) {
        this.key_solusi = key_solusi;
    }

    public String getLabel_kondisi() {
        return label_kondisi;
    }

    public void setLabel_kondisi(String label_kondisi) {
        this.label_kondisi = label_kondisi;
    }

    public String getLabel_solusi() {
        return label_solusi;
    }

    public void setLabel_solusi(String label_solusi) {
        this.label_solusi = label_solusi;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
