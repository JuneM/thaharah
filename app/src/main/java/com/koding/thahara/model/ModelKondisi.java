package com.koding.thahara.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class ModelKondisi implements Serializable {
    String kondisi;
    String bila_benar;
    String bila_salah;
    String start;
    String stop;
    String uid;
    String username;
    String key;

    public ModelKondisi(){

    }

    public ModelKondisi(String kondisi, String bila_benar, String bila_salah, String start, String stop, String uid, String username) {
        this.kondisi = kondisi;
        this.bila_benar = bila_benar;
        this.bila_salah = bila_salah;
        this.start = start;
        this.stop = stop;
        this.uid = uid;
        this.username = username;
        this.key = key;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public String getBila_benar() {
        return bila_benar;
    }

    public void setBila_benar(String bila_benar) {
        this.bila_benar = bila_benar;
    }

    public String getBila_salah() {
        return bila_salah;
    }

    public void setBila_salah(String bila_salah) {
        this.bila_salah = bila_salah;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
