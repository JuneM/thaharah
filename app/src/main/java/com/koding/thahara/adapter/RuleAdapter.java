package com.koding.thahara.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.koding.thahara.R;
import com.koding.thahara.model.ModelRule;
import com.koding.thahara.ui.DetailEdit.DetailRuleActivity;
import com.koding.thahara.ui.List.ListRuleActivity;

import java.util.List;

public class RuleAdapter extends RecyclerView.Adapter<RuleAdapter.ViewHolderRule> {

    private Context context;
    private List<ModelRule> modelRules;
    private static final String TAG_SESSION = "CekitSession";
    FirebaseDataListener listener;

    public RuleAdapter(Context context, List<ModelRule> modelRules) {
        this.context = context;
        this.modelRules = modelRules;
        this.listener = (ListRuleActivity) context;
    }

    @NonNull
    @Override
    public ViewHolderRule onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_rule, parent, false);
        return new ViewHolderRule(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderRule holder, final int position) {

        final ModelRule modelRule = modelRules.get(position);
        holder.tvIdRule.setText(String.valueOf(position + 1));
        SharedPreferences settings;
        settings = ((Activity) context).getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        final String uid = settings.getString("key", null);
        final String role = settings.getString("role",null);
        holder.setModel(modelRule);
        if (!uid.equals(modelRule.getUid())) {
            if (!role.equals("admin")){
                holder.rlIdItem.setBackgroundColor(context.getResources().getColor(R.color.grey_300));
            }
        }
        holder.rlIdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetail = new Intent(context, DetailRuleActivity.class);
                intentDetail.putExtra(DetailRuleActivity.EXTRA_DATA, modelRule);
                context.startActivity(intentDetail);
                ((Activity) context).finish();
            }
        });
        holder.ivMenuRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.ivMenuRule);
                popupMenu.inflate(R.menu.list_rule);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.action_detail:
                                Intent intentDetail = new Intent(context, DetailRuleActivity.class);
                                intentDetail.putExtra(DetailRuleActivity.EXTRA_DATA, modelRule);
                                context.startActivity(intentDetail);
                                ((Activity) context).finish();
                                break;
                            case R.id.action_hapus:
                                if (uid.equals(modelRule.getUid()) || role.equals("admin")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Hapus Data  ?");
                                    builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            listener.onDelete(modelRules.get(position), position);
                                        }
                                    });
                                    builder.show();
                                } else {
                                    Toast.makeText(context, "Maaf tidak bisa hapus karena anda bukan yang buat data ini", Toast.LENGTH_LONG).show();
                                }
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        if (modelRules != null) {
            return modelRules.size();
        } else {
            return 0;
        }
    }

    public class ViewHolderRule extends RecyclerView.ViewHolder {
        public ModelRule modelRuleList;
        private TextView tvIdRule, tvKondisi, tvSolusi;
        private ImageView ivMenuRule;
        private RelativeLayout rlIdItem;

        public ViewHolderRule(@NonNull View itemView) {
            super(itemView);
            tvIdRule = itemView.findViewById(R.id.tv_id_rule);
            tvKondisi = itemView.findViewById(R.id.tv_kondisi_value);
            tvSolusi = itemView.findViewById(R.id.tv_solusi_value);
            rlIdItem = itemView.findViewById(R.id.rl_item);
            ivMenuRule = itemView.findViewById(R.id.iv_menu_rule);
        }

        public void setModel(ModelRule model) {
            modelRuleList = model;
            tvKondisi.setText(modelRuleList.getLabel_kondisi());
            tvSolusi.setText(modelRuleList.getLabel_solusi());
        }
    }

    public interface FirebaseDataListener {
        void onDelete(ModelRule modelRule, int position);
    }
}
