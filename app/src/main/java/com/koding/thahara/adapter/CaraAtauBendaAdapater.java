package com.koding.thahara.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.koding.thahara.R;
import com.koding.thahara.model.ModelCaraAtauBenda;
import com.koding.thahara.ui.List.ListCaraAtauBendaActivity;
import com.koding.thahara.ui.DetailEdit.DetailCaraAtauBendaActivity;

import java.util.List;

public class CaraAtauBendaAdapater extends RecyclerView.Adapter<CaraAtauBendaAdapater.ViewHolderCaraAtauBenda> {
    private Context context;
    private List<ModelCaraAtauBenda> modelCaraAtauBendas;
    private static final String TAG_SESSION = "CekitSession";
    FirebaseDataListener listener;

    public CaraAtauBendaAdapater(Context context, List<ModelCaraAtauBenda> modelCaraAtauBendas) {
        this.context = context;
        this.modelCaraAtauBendas = modelCaraAtauBendas;
        this.listener = (ListCaraAtauBendaActivity) context;
    }

    @NonNull
    @Override
    public ViewHolderCaraAtauBenda onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_carataubenda, parent, false);
        return new ViewHolderCaraAtauBenda(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderCaraAtauBenda holder, final int position) {
        final ModelCaraAtauBenda modelCaraAtauBenda = modelCaraAtauBendas.get(position);
        holder.tvIdCaraAtauBenda.setText(String.valueOf(position + 1));
        SharedPreferences settings;
        settings = ((Activity) context).getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        final String uid = settings.getString("key", null);
        final String role = settings.getString("role", null);
        holder.setModel(modelCaraAtauBenda);
        if (!uid.equals(modelCaraAtauBenda.getUid())) {
            if (!role.equals("admin")){
                holder.rlIdItem.setBackgroundColor(context.getResources().getColor(R.color.grey_300));
            }
        }
        holder.rlIdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetail = new Intent(context, DetailCaraAtauBendaActivity.class);
                intentDetail.putExtra(DetailCaraAtauBendaActivity.EXTRA_DATA, modelCaraAtauBenda);
                context.startActivity(intentDetail);
                ((Activity) context).finish();
            }
        });
        holder.ivMenuCaraAtauBenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.ivMenuCaraAtauBenda);
                popupMenu.inflate(R.menu.list_cara_atau_benda);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.action_detail:
                                Intent intentDetail = new Intent(context, DetailCaraAtauBendaActivity.class);
                                intentDetail.putExtra(DetailCaraAtauBendaActivity.EXTRA_DATA, modelCaraAtauBenda);
                                context.startActivity(intentDetail);
                                ((Activity) context).finish();
                                break;
                            case R.id.action_hapus:
                                if (uid.equals(modelCaraAtauBenda.getUid()) || role.equals("admin")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Hapus Data  ?");
                                    builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            listener.onDelete(modelCaraAtauBendas.get(position), position);
                                        }
                                    });
                                    builder.show();
                                } else {
                                    Toast.makeText(context, "Maaf tidak bisa hapus karena anda bukan yang buat data ini", Toast.LENGTH_LONG).show();
                                }
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        if (modelCaraAtauBendas != null) {
            return modelCaraAtauBendas.size();
        } else {
            return 0;
        }
    }

    public class ViewHolderCaraAtauBenda extends RecyclerView.ViewHolder {
        public ModelCaraAtauBenda modelCaraAtauBendalist;
        private TextView tvIdCaraAtauBenda, tvSolusi, tvMazhab, tvUser, tvKeterangan;
        private ImageView ivMenuCaraAtauBenda;
        private RelativeLayout rlIdItem;

        public ViewHolderCaraAtauBenda(@NonNull View itemView) {
            super(itemView);
            tvIdCaraAtauBenda = itemView.findViewById(R.id.tv_id_cara_atau_benda);
            tvSolusi = itemView.findViewById(R.id.tv_solusi_value);
            ivMenuCaraAtauBenda = itemView.findViewById(R.id.iv_menu_cara_atau_benda);
            tvMazhab = itemView.findViewById(R.id.tv_mazhab_value);
            tvUser = itemView.findViewById(R.id.tv_user_value);
            rlIdItem = itemView.findViewById(R.id.rl_item);
        }

        public void setModel(ModelCaraAtauBenda modelCaraAtauBenda) {
            modelCaraAtauBendalist = modelCaraAtauBenda;
            tvSolusi.setText(modelCaraAtauBenda.getSolusi());
            tvMazhab.setText(modelCaraAtauBenda.getMazhab());
            tvUser.setText(modelCaraAtauBenda.getUsername());
        }
    }

    public interface FirebaseDataListener {
        void onDelete(ModelCaraAtauBenda modelCaraAtauBenda, int position);
    }
}
