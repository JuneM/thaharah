package com.koding.thahara.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.koding.thahara.R;
import com.koding.thahara.model.ModelArtikel;
import com.koding.thahara.ui.DetailEdit.DetailArtikelActivity;
import com.koding.thahara.ui.List.ListArtikelActivity;

import java.util.ArrayList;
import java.util.List;

public class ArtikelAdapter extends RecyclerView.Adapter<ArtikelAdapter.ViewHolderArtikel> {
    private Context context;
    private List<ModelArtikel> modelArtikels;
    private static final String TAG_SESSION = "CekitSession";
    ArtikelAdapter.FirebaseDataListener listener;

    public ArtikelAdapter(Context context, ArrayList<ModelArtikel> modelArtikels) {
        this.context = context;
        this.modelArtikels = modelArtikels;
        this.listener = (ListArtikelActivity) context;
    }

    @NonNull
    @Override
    public ViewHolderArtikel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_artikel, parent, false);
        return new ArtikelAdapter.ViewHolderArtikel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderArtikel holder, final int position) {
        final ModelArtikel modelArtikel = modelArtikels.get(position);
        holder.tvIdArtikel.setText(String.valueOf(position + 1));
        SharedPreferences settings;
        settings = ((Activity) context).getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        final String uid = settings.getString("key", null);
        final String role = settings.getString("role", null);
        holder.setModel(modelArtikel);
        if (!role.equals("admin")){
            holder.ivMenuArtikel.setVisibility(View.GONE);
        }

        holder.rlIdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetail = new Intent(context, DetailArtikelActivity.class);
                intentDetail.putExtra(DetailArtikelActivity.EXTRA_DATA, modelArtikel);
                context.startActivity(intentDetail);
                ((Activity) context).finish();
            }
        });

        holder.ivMenuArtikel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.ivMenuArtikel);
                popupMenu.inflate(R.menu.list_artikel);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.action_detail:
                                Intent intentDetail = new Intent(context, DetailArtikelActivity.class);
                                intentDetail.putExtra(DetailArtikelActivity.EXTRA_DATA, modelArtikel);
                                context.startActivity(intentDetail);
                                ((Activity) context).finish();
                                break;
                            case R.id.action_hapus:
                                if (role.equals("admin")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Hapus Data  ?");
                                    builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            listener.onDelete(modelArtikels.get(position), position);
                                        }
                                    });
                                    builder.show();
                                } else {
                                    Toast.makeText(context, "Maaf tidak bisa hapus karena anda bukan yang buat data ini", Toast.LENGTH_LONG).show();
                                }
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        if (modelArtikels != null) {
            return modelArtikels.size();
        } else {
            return 0;
        }
    }

    public class ViewHolderArtikel extends RecyclerView.ViewHolder {
        public ModelArtikel modelArtikelList;
        private TextView tvIdArtikel, tvJudulArtikel, tvUsername;
        private ImageView ivMenuArtikel;
        private RelativeLayout rlIdItem;
        public ViewHolderArtikel(@NonNull View itemView) {
            super(itemView);
            tvIdArtikel = itemView.findViewById(R.id.tv_id_artikel);
            tvJudulArtikel = itemView.findViewById(R.id.tv_judul_artikel);
            tvUsername = itemView.findViewById(R.id.tv_username);
            ivMenuArtikel = itemView.findViewById(R.id.iv_menu_artikel);
            rlIdItem = itemView.findViewById(R.id.rl_item);
        }

        public void setModel(ModelArtikel modelArtikel) {
            modelArtikelList = modelArtikel;
            tvJudulArtikel.setText(modelArtikelList.getJudulArtikel());
            tvUsername.setText(modelArtikelList.getUsername());
        }
    }

    public interface FirebaseDataListener {
        void onDelete(ModelArtikel modelArtikel, int position);
    }
}
