package com.koding.thahara.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.koding.thahara.R;
import com.koding.thahara.model.ModelKondisi;
import com.koding.thahara.ui.DetailEdit.DetailKondisiActivity;
import com.koding.thahara.ui.List.ListKondisiActivity;

import java.util.List;

public class KodisiAdapter extends RecyclerView.Adapter<KodisiAdapter.ViewHolderKondisi> {
    private Context context;
    private List<ModelKondisi> modelKondisis;
    private static final String TAG_SESSION = "CekitSession";
    FirebaseDataListener listener;

    public KodisiAdapter(Context context, List<ModelKondisi> modelKondisis) {
        this.context = context;
        this.modelKondisis = modelKondisis;
        this.listener = (ListKondisiActivity) context;
    }

    @NonNull
    @Override
    public ViewHolderKondisi onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_identifikasikondisi, parent, false);
        return new ViewHolderKondisi(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderKondisi holder, final int position) {
        final ModelKondisi modelKondisi = modelKondisis.get(position);
        holder.tvIdKondisi.setText(String.valueOf(position + 1));
        SharedPreferences settings;
        settings = ((Activity) context).getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        final String uid = settings.getString("key", null);
        final String role = settings.getString("role", null);
        holder.setModel(modelKondisi);
        if (!uid.equals(modelKondisi.getUid())) {
            if (!role.equals("admin")){
                holder.rlIdItem.setBackgroundColor(context.getResources().getColor(R.color.grey_300));
            }
        }

        holder.rlIdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetail = new Intent(context, DetailKondisiActivity.class);
                intentDetail.putExtra(DetailKondisiActivity.EXTRA_DATA, modelKondisi);
                context.startActivity(intentDetail);
                ((Activity) context).finish();
            }
        });
        holder.ivMenuKondisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.ivMenuKondisi);
                popupMenu.inflate(R.menu.list_kondisi);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.action_detail:
                                Intent intentDetail = new Intent(context, DetailKondisiActivity.class);
                                intentDetail.putExtra(DetailKondisiActivity.EXTRA_DATA, modelKondisi);
                                context.startActivity(intentDetail);
                                ((Activity) context).finish();
                                break;
                            case R.id.action_hapus:
                                if (uid.equals(modelKondisi.getUid()) || role.equals("admin")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Hapus Data  ?");
                                    builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            listener.onDelete(modelKondisis.get(position), position);
                                        }
                                    });
                                    builder.show();
                                } else {
                                    Toast.makeText(context, "Maaf tidak bisa hapus karena anda bukan yang buat data ini", Toast.LENGTH_LONG).show();
                                }
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        if (modelKondisis != null) {
            return modelKondisis.size();
        } else {
            return 0;
        }
    }

    public class ViewHolderKondisi extends RecyclerView.ViewHolder {
        public ModelKondisi modelKondisiList;
        private TextView tvIdKondisi, tvKondisi, tvUser;
        private ImageView ivMenuKondisi;
        private RelativeLayout rlIdItem;

        public ViewHolderKondisi(@NonNull View itemView) {
            super(itemView);
            tvIdKondisi = itemView.findViewById(R.id.tv_id_kondisi);
            tvKondisi = itemView.findViewById(R.id.tv_kondisi_value);
            tvUser = itemView.findViewById(R.id.tv_user_value);
            rlIdItem = itemView.findViewById(R.id.rl_item);
            ivMenuKondisi = itemView.findViewById(R.id.iv_menu_kondisi);
        }

        public void setModel(ModelKondisi model) {
            modelKondisiList = model;
            tvKondisi.setText(modelKondisiList.getKondisi());
            tvUser.setText(modelKondisiList.getUsername());
        }
    }

    public interface FirebaseDataListener {
        void onDelete(ModelKondisi modelKondisi, int position);
    }
}
