package com.koding.thahara.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelUser;
import com.koding.thahara.ui.List.ListDataUserActivity;

import java.util.List;

public class DataUserAdapter extends RecyclerView.Adapter<DataUserAdapter.ViewHolderDataUser> {

    private Context context;
    private List<ModelUser> modelUsers;
    FirebaseDataListener listener;

    public DataUserAdapter(Context context, List<ModelUser> modelUsers) {
        this.context = context;
        this.modelUsers = modelUsers;
        this.listener = (ListDataUserActivity) context;
    }

    @NonNull
    @Override
    public ViewHolderDataUser onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_datauser, parent, false);
        return new ViewHolderDataUser(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderDataUser holder, final int position) {
        final ModelUser users = modelUsers.get(position);
        holder.tvIdUser.setText(String.valueOf(position+1));
        holder.setModel(users);
        holder.ivMenuUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.ivMenuUser);
                popupMenu.inflate(R.menu.list_user);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_approve:
                                users.setStatus(true);
                                DatabaseReference mDatabase;
                                mDatabase = FirebaseDatabase.getInstance().getReference();
                                mDatabase.child("users")
                                        .child(users.getKey())
                                        .setValue(users);
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus Data  ?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        listener.onDelete(modelUsers.get(position), position);
                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (modelUsers != null){
            return modelUsers.size();
        }else{
            return 0;
        }
    }

    public class ViewHolderDataUser extends RecyclerView.ViewHolder {
        public  ModelUser modelUsersList;
        private TextView tvIdUser, tvNamaUser, tvStatusUser;
        private ImageView ivMenuUser;
        private RelativeLayout rlIdItem;
        public ViewHolderDataUser(@NonNull View itemView) {
            super(itemView);

            tvIdUser = itemView.findViewById(R.id.tv_id_user);
            tvNamaUser = itemView.findViewById(R.id.tv_nama_user);
            ivMenuUser = itemView.findViewById(R.id.iv_menu_user);
            tvStatusUser = itemView.findViewById(R.id.tv_status_verified);
            rlIdItem = itemView.findViewById(R.id.rl_item);
        }

        public void setModel(ModelUser users) {
            modelUsersList = users;
            tvNamaUser.setText(users.getUsername());
            tvStatusUser.setText(users.getStatus().toString());
        }

    }

    public interface FirebaseDataListener {
        void onDelete(ModelUser user, int position);
    }
}
