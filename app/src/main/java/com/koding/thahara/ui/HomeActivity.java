package com.koding.thahara.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.ui.List.ListArtikelActivity;
import com.koding.thahara.ui.List.ListCaraAtauBendaActivity;
import com.koding.thahara.ui.List.ListDataUserActivity;
import com.koding.thahara.ui.List.ListKondisiActivity;
import com.koding.thahara.ui.List.ListRuleActivity;
import com.koding.thahara.ui.auth.LoginActivity;


public class HomeActivity extends BaseActivity {
    private static final String TAG_SESSION = "CekitSession";
    private Button btnLogOut, btnBendaBersuci, btnKondisiBersuci, btnSettingsRule, btnDataUser, btnArtikel;
    private TextView tvUsername, mUsername, tvVerifiedStatus, mStatusVerified;
    private String role;
    private DatabaseReference mDatabase;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mDatabase = FirebaseDatabase.getInstance().getReference("users");
        setUpView();
        cekUserRole();
        toolListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }

    }

    private void setUpView() {
        mUsername = findViewById(R.id.tv_username);
        tvVerifiedStatus = findViewById(R.id.tv_status_verified_view);
        mStatusVerified = findViewById(R.id.tv_status_verified_value);
        btnLogOut = findViewById(R.id.btn_logout);
        btnBendaBersuci = findViewById(R.id.btn_benda_bersuci);
        btnKondisiBersuci = findViewById(R.id.btn_kondisi_bersuci);
        btnSettingsRule = findViewById(R.id.btn_settings_rule);
        btnDataUser = findViewById(R.id.btn_data_user);
        btnArtikel = findViewById(R.id.btn_artikel);
    }

    private void cekUserRole() {
        showProgressDialog();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        String status = settings.getString("status", null);
        String role = settings.getString("role", null);
        mUsername.setText(username);
        mStatusVerified.setText(status);
        mStatusVerified.setText(String.valueOf(status));
        if (role.equals("admin")){
            btnBendaBersuci.setVisibility(View.VISIBLE);
            btnKondisiBersuci.setVisibility(View.VISIBLE);
            btnSettingsRule.setVisibility(View.VISIBLE);
            btnDataUser.setVisibility(View.VISIBLE);
            btnArtikel.setVisibility(View.VISIBLE);
            if (status.equals("true")){
                btnBendaBersuci.setEnabled(true);
                btnKondisiBersuci.setEnabled(true);
                btnArtikel.setEnabled(true);
            }
        }else{
            tvVerifiedStatus.setVisibility(View.VISIBLE);
            mStatusVerified.setVisibility(View.VISIBLE);
            mStatusVerified.setText(String.valueOf(status));
            btnBendaBersuci.setVisibility(View.VISIBLE);
            btnKondisiBersuci.setVisibility(View.VISIBLE);
            btnSettingsRule.setVisibility(View.VISIBLE);
            btnArtikel.setVisibility(View.VISIBLE);
            btnDataUser.setVisibility(View.GONE);
            if (status.equals("true")){
                btnBendaBersuci.setEnabled(true);
                btnKondisiBersuci.setEnabled(true);
                btnSettingsRule.setEnabled(true);
                btnArtikel.setEnabled(true);
            }else {
                btnBendaBersuci.setEnabled(false);
                btnKondisiBersuci.setEnabled(false);
                btnSettingsRule.setEnabled(false);
                btnArtikel.setEnabled(false);
            }
        }
        hideProgressDialog();
    }

    private void toolListener() {
        btnDataUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDataUser = new Intent(getApplicationContext(), ListDataUserActivity.class);
                startActivity(intentDataUser);
            }
        });
        btnBendaBersuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBendaBersuci = new Intent(getApplicationContext(), ListCaraAtauBendaActivity.class);
                startActivity(intentBendaBersuci);
            }
        });
        btnKondisiBersuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentKondisiBersuci = new Intent(getApplicationContext(), ListKondisiActivity.class);
                startActivity(intentKondisiBersuci);
            }
        });
        btnSettingsRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentRule = new Intent(getApplicationContext(), ListRuleActivity.class);
                startActivity(intentRule);
            }
        });
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences settings;
                SharedPreferences.Editor editor;
                settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
                editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent intentLogout = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intentLogout);
                finish();
            }
        });
        btnArtikel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentArtikel = new Intent(getApplicationContext(), ListArtikelActivity.class);
                startActivity(intentArtikel);
            }
        });

    }

}
