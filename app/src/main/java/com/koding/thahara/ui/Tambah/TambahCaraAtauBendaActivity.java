package com.koding.thahara.ui.Tambah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelCaraAtauBenda;
import com.koding.thahara.ui.List.ListCaraAtauBendaActivity;
import com.koding.thahara.ui.auth.LoginActivity;

public class TambahCaraAtauBendaActivity extends BaseActivity {
    private static final String TAG_SESSION = "CekitSession";
    private EditText etSolusi, etMazhab, etSumber, etKeterangan;
    private Button btnTambah, btnCancel;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_cab);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        setUpFindView();
        setToolListener();
    }

    private void setToolListener() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBackCAB = new Intent(getApplicationContext(), ListCaraAtauBendaActivity.class);
                startActivity(intentBackCAB);
                finish();
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences settings;
                settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
                String solusi = etSolusi.getText().toString();
                String mazhab = etMazhab.getText().toString();
                String sumber = etSumber.getText().toString();
                String ket = etKeterangan.getText().toString();
                String uid = settings.getString("key", null);
                String username = settings.getString("username", null);
                if (!validateForm(solusi, mazhab, sumber, ket)) {
                    return;
                }
                ModelCaraAtauBenda newData = new ModelCaraAtauBenda(solusi, mazhab, sumber, ket, uid, username);
                insertNewData(newData);
                Intent intentBackCAB = new Intent(getApplicationContext(), ListCaraAtauBendaActivity.class);
                startActivity(intentBackCAB);
                finish();
            }
        });
    }

    private void insertNewData(ModelCaraAtauBenda newData) {
        mDatabase.child("cara_benda_bersuci")
                .push()
                .setValue(newData)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(TambahCaraAtauBendaActivity.this, "Data Berhasil Di daftarkan", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setUpFindView() {
        etSolusi = findViewById(R.id.et_solusi);
        etMazhab = findViewById(R.id.et_mazhab);
        etSumber = findViewById(R.id.et_sumber);
        etKeterangan = findViewById(R.id.et_keterangan);
        btnCancel = findViewById(R.id.btn_cancel);
        btnTambah = findViewById(R.id.btn_tambah);
    }

    private boolean validateForm(String solusi, String mazhab, String sumber, String ket) {
        boolean valid = true;

        if (TextUtils.isEmpty(solusi)) {
            etSolusi.setError("Required.");
            valid = false;
        } else {
            etSolusi.setError(null);
        }

        if (TextUtils.isEmpty(mazhab)) {
            etMazhab.setError("Required.");
            valid = false;
        } else {
            etMazhab.setError(null);
        }

        if (TextUtils.isEmpty(sumber)) {
            etSumber.setError("Required.");
            valid = false;
        } else {
            etSumber.setError(null);
        }

        if (TextUtils.isEmpty(ket)) {
            etKeterangan.setError("Required.");
            valid = false;
        } else {
            etKeterangan.setError(null);
        }

        return valid;
    }


}
