package com.koding.thahara.ui.Tambah;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelArtikel;
import com.koding.thahara.ui.List.ListArtikelActivity;
import com.koding.thahara.ui.auth.LoginActivity;

public class TambahArtikelActivity extends AppCompatActivity {

    private static final String TAG_SESSION = "CekitSession";
    private EditText etJudulArtikel, etIsiArtikel;
    private Button btnTambah, btnCancel;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_artikel);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        setUpFindView();
        setToolListener();
    }
    private void setToolListener() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBackArtikel = new Intent(getApplicationContext(), ListArtikelActivity.class);
                startActivity(intentBackArtikel);
                finish();
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences settings;
                settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
                String judulArtikel = etJudulArtikel.getText().toString();
                String isiArtikel = etIsiArtikel.getText().toString();
                String uid = settings.getString("key", null);
                String username = settings.getString("username", null);
                if (!validateForm(judulArtikel, isiArtikel)) {
                    return;
                }
                ModelArtikel newData = new ModelArtikel(judulArtikel, isiArtikel, uid, username);
                insertNewData(newData);
                Intent intentBackArtikel = new Intent(getApplicationContext(), ListArtikelActivity.class);
                startActivity(intentBackArtikel);
                finish();
            }
        });
    }

    private void insertNewData(ModelArtikel newData) {
        mDatabase.child("artikel")
                .push()
                .setValue(newData)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(TambahArtikelActivity.this, "Data Berhasil Di daftarkan", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setUpFindView() {
        etJudulArtikel = findViewById(R.id.et_judul_arikel);
        etIsiArtikel = findViewById(R.id.et_isi_artikel);
        btnCancel = findViewById(R.id.btn_cancel);
        btnTambah = findViewById(R.id.btn_tambah);
    }

    private boolean validateForm(String judulArtikel, String isiArtikel) {
        boolean valid = true;

        if (TextUtils.isEmpty(judulArtikel)) {
            etJudulArtikel.setError("Required.");
            valid = false;
        } else {
            etJudulArtikel.setError(null);
        }

        if (TextUtils.isEmpty(isiArtikel)) {
            etIsiArtikel.setError("Required.");
            valid = false;
        } else {
            etIsiArtikel.setError(null);
        }

        return valid;
    }

}
