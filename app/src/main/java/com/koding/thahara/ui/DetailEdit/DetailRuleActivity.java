package com.koding.thahara.ui.DetailEdit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelCaraAtauBenda;
import com.koding.thahara.model.ModelKondisi;
import com.koding.thahara.model.ModelRule;
import com.koding.thahara.ui.List.ListRuleActivity;
import com.koding.thahara.ui.auth.LoginActivity;

import java.util.ArrayList;

public class DetailRuleActivity extends BaseActivity {

    private static final String TAG_SESSION = "CekitSession";
    private ModelRule modelRule;
    public static final String EXTRA_DATA = "extra_data";
    private Spinner spKondisi, spSolusi;
    private ArrayList<String> arrLabelKondisi, arrKeyKondisi, arrLabelSolusi, arrKeySolusi;
    private ArrayList<ModelRule> listModelRules;
    private TextView tvTitleText;
    private ImageView btnEdit, btnCancel, btnSubmit;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()) {
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rule);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        modelRule = (ModelRule) getIntent().getSerializableExtra(EXTRA_DATA);
        setUpFindView();
        setUi();
        setFectData(modelRule);


    }

    private void setUi() {
        tvTitleText.setText("Detail Identifikasi Kondisi Untuk Bersuci");
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String uid = settings.getString("key", null);
        String role = settings.getString("role", null);
        if (uid.equals(modelRule.getUid()) || role.equals("admin"))
        {
            btnEdit.setVisibility(View.VISIBLE);
        }else{
            btnEdit.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.GONE);
        }
        spKondisi.setEnabled(false);
        spSolusi.setEnabled(false);
    }

    private void setFectData(final ModelRule modelRule) {
        mDatabase.child("identifikasi_kondisi").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                showProgressDialog();
                arrLabelKondisi = new ArrayList<>();
                arrKeyKondisi = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelKondisi modelKondisi = noteDataSnapshoot.getValue(ModelKondisi.class);
                    if (modelKondisi.getStop().equals("Ya")) {
                        arrLabelKondisi.add(modelKondisi.getKondisi());
                        arrKeyKondisi.add(noteDataSnapshoot.getKey());
                    }
                }
//                Log.d("array", arrLabelKondisi.toString());
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item, arrLabelKondisi);
                adapter.setDropDownViewResource(R.layout.spinner_item);
                spKondisi.setAdapter(adapter);
                setUpDataSpSolusi(arrLabelKondisi, modelRule);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpDataSpSolusi(final ArrayList<String> arrayValKondisi, final ModelRule modelRule) {
        mDatabase.child("cara_benda_bersuci").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                arrLabelSolusi = new ArrayList<>();
                arrKeySolusi = new ArrayList<>();
                /*arrLabelSolusi.add("Pilih Solusi");*/
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelCaraAtauBenda modelCaraAtauBenda = noteDataSnapshoot.getValue(ModelCaraAtauBenda.class);
                    arrLabelSolusi.add(modelCaraAtauBenda.getSolusi());
                    arrKeySolusi.add(noteDataSnapshoot.getKey());
                }
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item, arrLabelSolusi);
                adapter.setDropDownViewResource(R.layout.spinner_item);
                spSolusi.setAdapter(adapter);
                hideProgressDialog();
                String kondisi = modelRule.getLabel_kondisi();
                String solusi = modelRule.getLabel_solusi();
                int positionKondisi = arrLabelKondisi.indexOf(kondisi);
                int positionSolusi = arrLabelSolusi.indexOf(solusi);
                spKondisi.setSelection(positionKondisi);
                spSolusi.setSelection(positionSolusi);
                setToolListener(modelRule, arrayValKondisi, arrKeySolusi);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setToolListener(final ModelRule modelRule, ArrayList<String> arrValKondisi, ArrayList<String> arrValSolusi) {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTitleText.setText("Detail Rule");
                spKondisi.setEnabled(false);
                spSolusi.setEnabled(false);
                String kondisi = modelRule.getLabel_kondisi();
                String solusi = modelRule.getLabel_solusi();
                int positionKondisi = arrLabelKondisi.indexOf(kondisi);
                int positionSolusi = arrLabelSolusi.indexOf(solusi);
                spKondisi.setSelection(positionKondisi);
                spSolusi.setSelection(positionSolusi);
                btnCancel.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.GONE);
                btnEdit.setVisibility(View.VISIBLE);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTitleText.setText("Edit Rule");
                spKondisi.setEnabled(true);
                spSolusi.setEnabled(true);
                btnCancel.setVisibility(View.VISIBLE);
                btnEdit.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String kondisiVal = spKondisi.getSelectedItem().toString();
                final String solusiVal = spSolusi.getSelectedItem().toString();
                final String keyKondisi = arrKeyKondisi.get(spKondisi.getSelectedItemPosition());
                final String keySolusi = arrKeySolusi.get(spSolusi.getSelectedItemPosition());
                mDatabase = FirebaseDatabase.getInstance().getReference("aturan");
                mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        listModelRules = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelRule modelRule = noteDataSnapshoot.getValue(ModelRule.class);
                            modelRule.setKey(noteDataSnapshoot.getKey());
                            listModelRules.add(modelRule);
                        }
                        if (!validateRule(keyKondisi, listModelRules)) {
                            modelRule.setLabel_kondisi(kondisiVal);
                            modelRule.setKey_kondisi(keyKondisi);
                            modelRule.setLabel_solusi(solusiVal);
                            modelRule.setKey_solusi(keySolusi);
                            mDatabase.child(modelRule.getKey())
                                    .setValue(modelRule);
                            Toast.makeText(DetailRuleActivity.this, "Data Berhasil di ubah", Toast.LENGTH_SHORT).show();
                            Intent intentBackRule = new Intent(getApplicationContext(), ListRuleActivity.class);
                            startActivity(intentBackRule);
                            finish();
                        } else if (keyKondisi.contentEquals(modelRule.getKey_kondisi())) {
                            modelRule.setLabel_kondisi(kondisiVal);
                            modelRule.setKey_kondisi(keyKondisi);
                            modelRule.setLabel_solusi(solusiVal);
                            modelRule.setKey_solusi(keySolusi);
                            mDatabase.child(modelRule.getKey())
                                    .setValue(modelRule);
                            Toast.makeText(DetailRuleActivity.this, "Data Berhasil di ubah", Toast.LENGTH_SHORT).show();
                            Intent intentBackRule = new Intent(getApplicationContext(), ListRuleActivity.class);
                            startActivity(intentBackRule);
                            finish();
                        } else {
                            Toast.makeText(DetailRuleActivity.this, "data sudah ada", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
    }

    private boolean validateRule(String kondisi, @org.jetbrains.annotations.NotNull ArrayList<ModelRule> arrayRule) {
        for (int i = 0; i < arrayRule.size(); i++) {
            if (kondisi.contentEquals(arrayRule.get(i).getKey_kondisi())) {
                return true;
            }
        }
        return false;
    }

    private void setUpFindView() {
        spKondisi = findViewById(R.id.sp_kondisi);
        spSolusi = findViewById(R.id.sp_solusi);
        btnCancel = findViewById(R.id.iv_cancel_rule);
        btnEdit = findViewById(R.id.iv_edit_rule);
        btnSubmit = findViewById(R.id.iv_submit_rule);
        tvTitleText = findViewById(R.id.tv_title_list_rule);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentBackRule = new Intent(getApplicationContext(), ListRuleActivity.class);
        startActivity(intentBackRule);
        finish();
    }

}
