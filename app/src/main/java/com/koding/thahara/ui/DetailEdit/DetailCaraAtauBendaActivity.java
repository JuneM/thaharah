package com.koding.thahara.ui.DetailEdit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelCaraAtauBenda;
import com.koding.thahara.ui.List.ListCaraAtauBendaActivity;
import com.koding.thahara.ui.auth.LoginActivity;

public class DetailCaraAtauBendaActivity extends AppCompatActivity {

    private static final String TAG_SESSION = "CekitSession";
    private ModelCaraAtauBenda modelCaraAtauBenda;
    public static final String EXTRA_DATA = "extra_data";
    private TextView tvTitleText;
    private EditText etSolusi, etMazhab, etSumber, etKeterangan;
    private ImageView btnEdit, btnCancel, btnSubmit;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_cab);
        modelCaraAtauBenda = (ModelCaraAtauBenda) getIntent().getSerializableExtra(EXTRA_DATA);
        setUpFindView();
        setUi(modelCaraAtauBenda);
        setFetchData(modelCaraAtauBenda);
        setToolListener(modelCaraAtauBenda);
    }

    private void setUi(ModelCaraAtauBenda modelCaraAtauBenda) {
        tvTitleText.setText("Detail Cara Atau Benda Bersuci");
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String uid = settings.getString("key", null);
        String role = settings.getString("role", null);
        if (uid.equals(modelCaraAtauBenda.getUid()) || role.equals("admin"))
        {
            btnEdit.setVisibility(View.VISIBLE);
        }else{
            btnEdit.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.GONE);
        }
    }

    private void setFetchData(ModelCaraAtauBenda modelCaraAtauBenda) {
        etSolusi.setText(modelCaraAtauBenda.getSolusi());
        etMazhab.setText(modelCaraAtauBenda.getMazhab());
        etSumber.setText(modelCaraAtauBenda.getSumber());
        etKeterangan.setText(modelCaraAtauBenda.getKeterangan());
    }

    private void setToolListener(final ModelCaraAtauBenda modelCaraAtauBenda) {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTitleText.setText("Detail Cara Atau Benda Bersuci");
                etSolusi.setEnabled(false);
                etMazhab.setEnabled(false);
                etSumber.setEnabled(false);
                etKeterangan.setEnabled(false);
                etSolusi.setText(modelCaraAtauBenda.getSolusi());
                etMazhab.setText(modelCaraAtauBenda.getMazhab());
                etSumber.setText(modelCaraAtauBenda.getSumber());
                etKeterangan.setText(modelCaraAtauBenda.getKeterangan());
                btnCancel.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.GONE);
                btnEdit.setVisibility(View.VISIBLE);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTitleText.setText("Edit Cara Atau Benda Bersuci");
                etSolusi.setEnabled(true);
                etMazhab.setEnabled(true);
                etSumber.setEnabled(true);
                etKeterangan.setEnabled(true);
                btnCancel.setVisibility(View.VISIBLE);
                btnEdit.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String solusi = etSolusi.getText().toString();
                String mazhab = etMazhab.getText().toString();
                String sumber = etSumber.getText().toString();
                String ket = etKeterangan.getText().toString();
                if (!validateForm(solusi, mazhab, sumber, ket)) {
                    return;
                }
                mDatabase = FirebaseDatabase.getInstance().getReference("cara_benda_bersuci");
                modelCaraAtauBenda.setSolusi(etSolusi.getText().toString());
                modelCaraAtauBenda.setMazhab(etMazhab.getText().toString());
                modelCaraAtauBenda.setSumber(etSumber.getText().toString());
                modelCaraAtauBenda.setKeterangan(etKeterangan.getText().toString());
                mDatabase.child(modelCaraAtauBenda.getKey())
                        .setValue(modelCaraAtauBenda);
                Toast.makeText(DetailCaraAtauBendaActivity.this, "Data Berhasil di ubah", Toast.LENGTH_SHORT).show();
                Intent intentBackCAB = new Intent(getApplicationContext(), ListCaraAtauBendaActivity.class);
                startActivity(intentBackCAB);
                finish();

            }
        });
    }

    private void setUpFindView() {
        etSolusi = findViewById(R.id.et_solusi);
        etMazhab = findViewById(R.id.et_mazhab);
        etSumber = findViewById(R.id.et_sumber);
        etKeterangan = findViewById(R.id.et_keterangan);
        btnCancel = findViewById(R.id.iv_cancel_cara_atau_benda);
        btnEdit = findViewById(R.id.iv_edit_cara_atau_benda);
        btnSubmit = findViewById(R.id.iv_submit_cara_atau_benda);
        tvTitleText = findViewById(R.id.tv_title_list_cab);
    }

    private boolean validateForm(String solusi, String mazhab, String sumber, String ket) {
        boolean valid = true;

        if (TextUtils.isEmpty(solusi)) {
            etSolusi.setError("Required.");
            valid = false;
        } else {
            etSolusi.setError(null);
        }

        if (TextUtils.isEmpty(mazhab)) {
            etMazhab.setError("Required.");
            valid = false;
        } else {
            etMazhab.setError(null);
        }

        if (TextUtils.isEmpty(sumber)) {
            etSumber.setError("Required.");
            valid = false;
        } else {
            etSumber.setError(null);
        }

        if (TextUtils.isEmpty(ket)) {
            etKeterangan.setError("Required.");
            valid = false;
        } else {
            etKeterangan.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentBackCAB = new Intent(getApplicationContext(), ListCaraAtauBendaActivity.class);
        startActivity(intentBackCAB);
        finish();
    }
}
