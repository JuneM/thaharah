package com.koding.thahara.ui.Tambah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelCaraAtauBenda;
import com.koding.thahara.model.ModelKondisi;
import com.koding.thahara.model.ModelRule;
import com.koding.thahara.ui.List.ListRuleActivity;
import com.koding.thahara.ui.auth.LoginActivity;

import java.util.ArrayList;

public class TambahRuleActivity extends BaseActivity {
    private static final String TAG_SESSION = "CekitSession";

    private Spinner spKondisi, spSolusi;
    private ArrayList<String> arraySpKondisi;
    private ArrayList<String> arrayValKondisi;
    private ArrayList<String> arraySpSolusi;
    private ArrayList<String> arrayValSolusi;
    private ArrayList<ModelRule> listModelRules;
    private Button btnTambah, btnCancel;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()) {
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_rule);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        setUpFindView();
        setUpDataSpKondisi();
        setToolListener();
    }

    private void setUpDataSpKondisi() {
        mDatabase.child("identifikasi_kondisi").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                showProgressDialog();
                arraySpKondisi = new ArrayList<>();
                arrayValKondisi = new ArrayList<>();
                /*arraySpKondisi.add("Pilih Kondisi");*/
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelKondisi modelKondisi = noteDataSnapshoot.getValue(ModelKondisi.class);
                    if (modelKondisi.getStop().equals("Ya")){
                        arraySpKondisi.add(modelKondisi.getKondisi());
                        arrayValKondisi.add(noteDataSnapshoot.getKey());
                    }
                }
                Log.d("array", arraySpKondisi.toString());
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item, arraySpKondisi);
                adapter.setDropDownViewResource(R.layout.spinner_item);
                spKondisi.setAdapter(adapter);
                setUpDataSpSolusi(arrayValKondisi);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpDataSpSolusi(final ArrayList<String> arrayValKondisi) {
        mDatabase.child("cara_benda_bersuci").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                arraySpSolusi = new ArrayList<>();
                arrayValSolusi = new ArrayList<>();
                /*arraySpSolusi.add("Pilih Solusi");*/
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelCaraAtauBenda modelCaraAtauBenda = noteDataSnapshoot.getValue(ModelCaraAtauBenda.class);
                    arraySpSolusi.add(modelCaraAtauBenda.getSolusi());
                    arrayValSolusi.add(noteDataSnapshoot.getKey());
                }
                Log.d("array", arraySpSolusi.toString());
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item, arraySpSolusi);
                adapter.setDropDownViewResource(R.layout.spinner_item);
                spSolusi.setAdapter(adapter);
                hideProgressDialog();
                btnTambahListener(arrayValKondisi, arrayValSolusi);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void btnTambahListener(final ArrayList<String> arrayValKondisi, final ArrayList<String> arrayValSolusi) {
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences settings;
                settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
                final String keyKondisi = arrayValKondisi.get(spKondisi.getSelectedItemPosition());
                final String keySolusi = arrayValSolusi.get(spSolusi.getSelectedItemPosition());
                final String labelKondisi = spKondisi.getSelectedItem().toString();
                final String labelSolusi = spSolusi.getSelectedItem().toString();
                final String uid = settings.getString("key", null);
                final String username = settings.getString("username", null);
                mDatabase.child("aturan").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        listModelRules = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelRule modelRule = noteDataSnapshoot.getValue(ModelRule.class);
                            modelRule.setKey(noteDataSnapshoot.getKey());
                            listModelRules.add(modelRule);
                        }

                        if (!validateRule(keyKondisi, keySolusi, listModelRules)) {
                            ModelRule newData = new ModelRule(keyKondisi, keySolusi, labelKondisi, labelSolusi, uid, username);
                            insertNewData(newData);
                            Intent intentBack = new Intent(getApplicationContext(), ListRuleActivity.class);
                            startActivity(intentBack);
                            finish();
                        } else {
                            Toast.makeText(TambahRuleActivity.this, "Data Sudah Ada", Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }


    private boolean validateRule(String kondisi, String solusi, ArrayList<ModelRule> arrayRule) {
        for (int i = 0; i < arrayRule.size(); i++) {
            if (kondisi.contentEquals(arrayRule.get(i).getKey_kondisi())) {
                return true;
            }
        }
        return false;
    }


    private void setToolListener() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBackRule = new Intent(getApplicationContext(), ListRuleActivity.class);
                startActivity(intentBackRule);
                finish();
            }
        });
    }

    private void insertNewData(ModelRule newData) {
        mDatabase.child("aturan")
                .push()
                .setValue(newData)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(TambahRuleActivity.this, "Data Berhasil Di daftarkan", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setUpFindView() {
        spKondisi = findViewById(R.id.sp_kondisi);
        spSolusi = findViewById(R.id.sp_solusi);
        btnCancel = findViewById(R.id.btn_cancel);
        btnTambah = findViewById(R.id.btn_tambah);
    }
}
