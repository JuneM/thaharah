package com.koding.thahara.ui.Tambah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelKondisi;
import com.koding.thahara.ui.List.ListKondisiActivity;
import com.koding.thahara.ui.auth.LoginActivity;

import java.util.ArrayList;

public class TambahKondisiActivity extends AppCompatActivity {
    private static final String TAG_SESSION = "CekitSession";
    private EditText etKondisi;
    private Spinner spBilaBenar, spBilaSalah, spStart, spStop;
    private ArrayList<String> arrLabelKondisi, arrKeyKondisi, arrYesNo;
    private ArrayList<ModelKondisi> arrModelKondisi;
    private Button btnTambah, btnCancel;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()) {
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_kondisi);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        setUpFindView();
        setUpData();
        setToolListener();
    }

    private void setUpData() {
        mDatabase.child("identifikasi_kondisi").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                arrLabelKondisi = new ArrayList<>();
                arrLabelKondisi.add("Pilih Bila Kondisi");
                arrKeyKondisi = new ArrayList<>();
                arrKeyKondisi.add("1");
                arrModelKondisi = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelKondisi modelKondisi = noteDataSnapshoot.getValue(ModelKondisi.class);
                    arrLabelKondisi.add(modelKondisi.getKondisi());
                    arrKeyKondisi.add(noteDataSnapshoot.getKey());
                    modelKondisi.setKey(noteDataSnapshoot.getKey());
                    arrModelKondisi.add(modelKondisi);
                }
//                Log.d("array", arrLabelKondisi.toString());
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item, arrLabelKondisi);
                adapter.setDropDownViewResource(R.layout.spinner_item);
                spBilaBenar.setAdapter(adapter);
                spBilaSalah.setAdapter(adapter);

                arrYesNo = new ArrayList<>();
                arrYesNo.add("Ya");
                arrYesNo.add("Tidak");
                final ArrayAdapter<String> adapterB = new ArrayAdapter<>(getApplicationContext(),
                        R.layout.spinner_item, arrYesNo);
                adapterB.setDropDownViewResource(R.layout.spinner_item);
                spStart.setAdapter(adapterB);
                spStop.setAdapter(adapterB);

                if (validateStart(arrModelKondisi)) {
                    spStart.setEnabled(false);
                    spStart.setSelection(1);
                }

                simpanData(arrModelKondisi, arrKeyKondisi);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void simpanData(final ArrayList<ModelKondisi> arrModelKondisi, final ArrayList<String> arrKeyKondisi) {
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences settings;
                String kondisi = etKondisi.getText().toString();
                settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
                String bilaBenar = arrKeyKondisi.get(spBilaBenar.getSelectedItemPosition());
                String bilaSalah = arrKeyKondisi.get(spBilaSalah.getSelectedItemPosition());
                String start = spStart.getSelectedItem().toString();
                String stop = spStop.getSelectedItem().toString();
                String uid = settings.getString("key", null);
                String username = settings.getString("username", null);
                if (!validateForm(kondisi)) {
                    return;
                }
                ModelKondisi newData = new ModelKondisi(kondisi, bilaBenar, bilaSalah, start, stop, uid, username);
                insertNewData(newData);
                Intent intentBackKondisi = new Intent(getApplicationContext(), ListKondisiActivity.class);
                startActivity(intentBackKondisi);
                finish();
            }
        });
    }

    private void setToolListener() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBackKondisi = new Intent(getApplicationContext(), ListKondisiActivity.class);
                startActivity(intentBackKondisi);
                finish();
            }
        });
    }

    private void insertNewData(ModelKondisi newData) {
        mDatabase.child("identifikasi_kondisi")
                .push()
                .setValue(newData)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(TambahKondisiActivity.this, "Data Berhasil Di daftarkan", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setUpFindView() {
        etKondisi = findViewById(R.id.et_kondisi);
        spBilaBenar = findViewById(R.id.sp_benar);
        spBilaSalah = findViewById(R.id.sp_salah);
        spStart = findViewById(R.id.sp_start);
        spStop = findViewById(R.id.sp_stop);
        btnCancel = findViewById(R.id.btn_cancel);
        btnTambah = findViewById(R.id.btn_tambah);
    }

    private boolean validateStart(ArrayList<ModelKondisi> data) {
        for (int i = 0; i < data.size(); i++) {
            if ("Ya".contentEquals(data.get(i).getStart())) {
                return true;
            }
        }
        return false;
    }

    private boolean validateForm(String kondisi) {
        boolean valid = true;

        if (TextUtils.isEmpty(kondisi)) {
            etKondisi.setError("Required.");
            valid = false;
        } else {
            etKondisi.setError(null);
        }

        return valid;
    }
}
