package com.koding.thahara.ui.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.adapter.CaraAtauBendaAdapater;
import com.koding.thahara.model.ModelCaraAtauBenda;
import com.koding.thahara.ui.Tambah.TambahArtikelActivity;
import com.koding.thahara.ui.auth.LoginActivity;

import java.util.ArrayList;

public class ListCaraAtauBendaActivity extends BaseActivity implements CaraAtauBendaAdapater.FirebaseDataListener {
    private static final String TAG_SESSION = "CekitSession";
    private DatabaseReference mDatabase;
    private ArrayList<ModelCaraAtauBenda> modelCaraAtauBendas;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView ivTambahCaraAtauBenda;
    private TextView tvSearch;
    private SearchView svSeacrh;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cara_atau_benda);
        FirebaseApp.initializeApp(this);
        setUpFindView();
        setRecycleView();
        setTambah();
    }

    private void setUpFindView() {
        recyclerView = findViewById(R.id.rv_list_CAB);
        ivTambahCaraAtauBenda = findViewById(R.id.iv_tambah_cara_atau_benda);
        tvSearch = findViewById(R.id.tv_search);
        svSeacrh = findViewById(R.id.search);
        svSeacrh.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() != 0){
                    tvSearch.setVisibility(View.GONE);
                }else{
                    tvSearch.setVisibility(View.VISIBLE);
                }
                recyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                mDatabase = FirebaseDatabase.getInstance().getReference();
                showProgressDialog();
                mDatabase.child("cara_benda_bersuci").orderByChild("solusi").startAt(query).endAt(query + "\uf8ff").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        modelCaraAtauBendas = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelCaraAtauBenda caraAtauBenda = noteDataSnapshoot.getValue(ModelCaraAtauBenda.class);
                            caraAtauBenda.setKey(noteDataSnapshoot.getKey());
                            modelCaraAtauBendas.add(caraAtauBenda);
                        }
                        adapter = new CaraAtauBendaAdapater(ListCaraAtauBendaActivity.this, modelCaraAtauBendas);
                        recyclerView.setAdapter(adapter);
                        hideProgressDialog();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                recyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                mDatabase = FirebaseDatabase.getInstance().getReference();
                showProgressDialog();
                mDatabase.child("cara_benda_bersuci").orderByChild("solusi").startAt(newText).endAt(newText + "\uf8ff").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        modelCaraAtauBendas = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelCaraAtauBenda caraAtauBenda = noteDataSnapshoot.getValue(ModelCaraAtauBenda.class);
                            caraAtauBenda.setKey(noteDataSnapshoot.getKey());
                            modelCaraAtauBendas.add(caraAtauBenda);
                        }
                        adapter = new CaraAtauBendaAdapater(ListCaraAtauBendaActivity.this, modelCaraAtauBendas);
                        recyclerView.setAdapter(adapter);
                        hideProgressDialog();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });
                if (newText.length() != 0){
                    tvSearch.setVisibility(View.GONE);
                }else{
                    tvSearch.setVisibility(View.VISIBLE);
                }

                return true;
            }
        });
    }

    private void setTambah() {
        ivTambahCaraAtauBenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentTambahArtikel = new Intent(getApplicationContext(), TambahArtikelActivity.class);
                startActivity(intentTambahArtikel);
                finish();
            }
        });
    }

    private void setRecycleView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        showProgressDialog();
        mDatabase.child("cara_benda_bersuci").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                modelCaraAtauBendas = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelCaraAtauBenda caraAtauBenda = noteDataSnapshoot.getValue(ModelCaraAtauBenda.class);
                    caraAtauBenda.setKey(noteDataSnapshoot.getKey());
                    modelCaraAtauBendas.add(caraAtauBenda);
                }
                adapter = new CaraAtauBendaAdapater(ListCaraAtauBendaActivity.this, modelCaraAtauBendas);
                recyclerView.setAdapter(adapter);
                hideProgressDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDelete(ModelCaraAtauBenda modelCaraAtauBenda, int position) {
        if (mDatabase != null) {
            mDatabase.child("cara_benda_bersuci").child(modelCaraAtauBenda.getKey()).removeValue();
        }

    }

}
