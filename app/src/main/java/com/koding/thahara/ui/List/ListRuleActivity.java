package com.koding.thahara.ui.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.adapter.RuleAdapter;
import com.koding.thahara.model.ModelRule;
import com.koding.thahara.ui.Tambah.TambahRuleActivity;
import com.koding.thahara.ui.auth.LoginActivity;

import java.util.ArrayList;

public class ListRuleActivity extends BaseActivity implements RuleAdapter.FirebaseDataListener {

    private static final String TAG_SESSION = "CekitSession";
    private DatabaseReference mDatabase;
    private ArrayList<ModelRule> modelRules;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView ivTambahRule;
    private TextView tvSearch;
    private SearchView svSeacrh;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rule);
        FirebaseApp.initializeApp(this);
        setUpFindView();
        setRecycleView();
        setTambah();
    }

    private void setUpFindView() {
        recyclerView = findViewById(R.id.rv_list_rule);
        ivTambahRule = findViewById(R.id.iv_tambah_rule);
        tvSearch = findViewById(R.id.tv_search);
        svSeacrh = findViewById(R.id.search);
        svSeacrh.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() != 0){
                    tvSearch.setVisibility(View.GONE);
                }else{
                    tvSearch.setVisibility(View.VISIBLE);
                }
                recyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                mDatabase = FirebaseDatabase.getInstance().getReference();
                showProgressDialog();
                mDatabase.child("aturan").orderByChild("label_kondisi").startAt(query).endAt(query + "\uf8ff").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        modelRules = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelRule modelRule = noteDataSnapshoot.getValue(ModelRule.class);
                            modelRule.setKey(noteDataSnapshoot.getKey());
                            modelRules.add(modelRule);
                        }
                        adapter = new RuleAdapter(ListRuleActivity.this, modelRules);
                        recyclerView.setAdapter(adapter);
                        hideProgressDialog();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() != 0){
                    tvSearch.setVisibility(View.GONE);
                }else{
                    tvSearch.setVisibility(View.VISIBLE);
                }
                recyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                mDatabase = FirebaseDatabase.getInstance().getReference();
                showProgressDialog();
                mDatabase.child("aturan").orderByChild("label_kondisi").startAt(newText).endAt(newText + "\uf8ff").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        modelRules = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelRule modelRule = noteDataSnapshoot.getValue(ModelRule.class);
                            modelRule.setKey(noteDataSnapshoot.getKey());
                            modelRules.add(modelRule);
                        }
                        adapter = new RuleAdapter(ListRuleActivity.this, modelRules);
                        recyclerView.setAdapter(adapter);
                        hideProgressDialog();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }
        });
    }

    private void setTambah() {
        ivTambahRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentTambahRule = new Intent(getApplicationContext(), TambahRuleActivity.class);
                startActivity(intentTambahRule);
                finish();
            }
        });
    }

    private void setRecycleView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        showProgressDialog();
        mDatabase.child("aturan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                modelRules = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelRule modelRule = noteDataSnapshoot.getValue(ModelRule.class);
                    modelRule.setKey(noteDataSnapshoot.getKey());
                    modelRules.add(modelRule);
                }
                adapter = new RuleAdapter(ListRuleActivity.this, modelRules);
                recyclerView.setAdapter(adapter);
                hideProgressDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onDelete(ModelRule modelRule, int position) {
        if (mDatabase != null) {
            mDatabase.child("aturan").child(modelRule.getKey()).removeValue();
        }
    }
}
