package com.koding.thahara.ui.DetailEdit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelKondisi;
import com.koding.thahara.ui.List.ListKondisiActivity;
import com.koding.thahara.ui.auth.LoginActivity;

import java.util.ArrayList;

public class DetailKondisiActivity extends BaseActivity {

    private static final String TAG_SESSION = "CekitSession";
    public static final String EXTRA_DATA = "extra_data";
    private TextView tvTitleText;
    private EditText etKondisi;
    private Spinner spBilaBenar, spBilaSalah, spStart, spStop;
    private ArrayList<String> arrLabelKondisi, arrKeyKondisi, arrYesNo;
    private ArrayList<ModelKondisi> arrModelKondisi;
    private ImageView btnEdit, btnCancel, btnSubmit;
    private ModelKondisi modelKondisi;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kondisi);
        modelKondisi = (ModelKondisi) getIntent().getSerializableExtra(EXTRA_DATA);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        setUpFindView();
        setUi(modelKondisi);
        setFectData(modelKondisi);
    }

    private void setUi(ModelKondisi modelKondisi) {
        tvTitleText.setText("Detail Identifikasi Kondisi Untuk Bersuci");
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String uid = settings.getString("key", null);
        String role = settings.getString("role", null);
        if (uid.equals(modelKondisi.getUid()) || role.equals("admin"))
        {
            btnEdit.setVisibility(View.VISIBLE);
        }else{
            btnEdit.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.GONE);
        }
        etKondisi.setEnabled(false);
        spBilaBenar.setEnabled(false);
        spBilaSalah.setEnabled(false);
        spStart.setEnabled(false);
        spStop.setEnabled(false);
    }

    private void setFectData(final ModelKondisi modelKondisi) {
        etKondisi.setText(modelKondisi.getKondisi());
        /*set isi spinner bila benar, salah, start dan stop*/
        showProgressDialog();
        mDatabase.child("identifikasi_kondisi").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //set isi array bila benar dan salah
                arrLabelKondisi = new ArrayList<>();
                arrLabelKondisi.add("Pilih Bila Kondisi");
                arrKeyKondisi = new ArrayList<>();
                arrKeyKondisi.add("1");
                arrModelKondisi = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelKondisi modelKondisi = noteDataSnapshoot.getValue(ModelKondisi.class);
                    arrLabelKondisi.add(modelKondisi.getKondisi());
                    arrKeyKondisi.add(noteDataSnapshoot.getKey());
                    modelKondisi.setKey(noteDataSnapshoot.getKey());
                    arrModelKondisi.add(modelKondisi);
                }
                //set isi array start dan stop
                arrYesNo = new ArrayList<>();
                arrYesNo.add("Ya");
                arrYesNo.add("Tidak");
                //set adapter bila benar dan salah
                setSpinnerA(arrLabelKondisi);
                hideProgressDialog();
                //set adapter start dan stop
                setSpinnerB(arrYesNo);
                //set bilaBenar, bilaSalah, start dan stop yang akan ditampilkan pertama kali
                String bilaBenar = modelKondisi.getBila_benar();
                String bilaSalah = modelKondisi.getBila_salah();
                String start = modelKondisi.getStart();
                String stop = modelKondisi.getStop();
                int positionBenar = arrKeyKondisi.indexOf(bilaBenar);
                int positionSalah = arrKeyKondisi.indexOf(bilaSalah);
                int positionStart = arrYesNo.indexOf(start);
                int positionStop = arrYesNo.indexOf(stop);
                spBilaBenar.setSelection(positionBenar);
                spBilaSalah.setSelection(positionSalah);
                spStart.setSelection(positionStart);
                spStop.setSelection(positionStop);

                setToolListener(modelKondisi, arrKeyKondisi, arrYesNo, arrModelKondisi);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
        /*end set isi spinner bila benar, salah, start dan stop*/
    }

    private void setSpinnerB(ArrayList<String> arrYesNo) {
        final ArrayAdapter<String> adapterB = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_item, arrYesNo);
        adapterB.setDropDownViewResource(R.layout.spinner_item);
        spStart.setAdapter(adapterB);
        spStop.setAdapter(adapterB);
    }

    private void setSpinnerA(ArrayList<String> arrLabelKondisi) {
        final ArrayAdapter<String> adapterA = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, arrLabelKondisi);
        adapterA.setDropDownViewResource(R.layout.spinner_item);
        spBilaBenar.setAdapter(adapterA);
        spBilaSalah.setAdapter(adapterA);
    }

    private void setToolListener(final ModelKondisi data, final ArrayList<String> arraySpinner, final ArrayList<String> item, final ArrayList<ModelKondisi> modelKondisis) {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTitleText.setText("Detail Identifikasi Kondisi Untuk Bersuci");
                etKondisi.setEnabled(false);
                spBilaBenar.setEnabled(false);
                spBilaSalah.setEnabled(false);
                spStart.setEnabled(false);
                spStop.setEnabled(false);
                etKondisi.setText(modelKondisi.getKondisi());
                String bilaBenar = data.getBila_benar();
                String bilaSalah = data.getBila_salah();
                String start = data.getStart();
                String stop = data.getStop();
                int positionBenar = arraySpinner.indexOf(bilaBenar);
                int positionSalah = arraySpinner.indexOf(bilaSalah);
                int positionStart = item.indexOf(start);
                int positionStop = item.indexOf(stop);
                spBilaBenar.setSelection(positionBenar);
                spBilaSalah.setSelection(positionSalah);
                spStart.setSelection(positionStart);
                spStop.setSelection(positionStop);
                btnCancel.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.GONE);
                btnEdit.setVisibility(View.VISIBLE);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTitleText.setText("Edit Identifikasi Kondisi Untuk Bersuci");
                etKondisi.setEnabled(true);
                spBilaBenar.setEnabled(true);
                spBilaSalah.setEnabled(true);
                if (validateStart(arrModelKondisi)) {
                    if (modelKondisi.getKey().equals(getStartVal(arrModelKondisi))){
                        spStart.setEnabled(true);
                    }else{
                        spStart.setEnabled(false);
                    }
                }else{
                    spStart.setEnabled(true);
                }
                spStop.setEnabled(true);
                btnCancel.setVisibility(View.VISIBLE);
                btnEdit.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String kondisi = etKondisi.getText().toString();
                String bilaBenar = spBilaBenar.getSelectedItem().toString();
                String bilaSalah = spBilaSalah.getSelectedItem().toString();
                String start = spStart.getSelectedItem().toString();
                String stop = spStop.getSelectedItem().toString();
                if (!validateForm(kondisi)) {
                    return;
                }
                if (kondisi.equals(bilaBenar)){
                    Toast.makeText(DetailKondisiActivity.this, "tidak boleh sama", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (kondisi.equals(bilaSalah)){
                    Toast.makeText(DetailKondisiActivity.this, "tidak boleh sama", Toast.LENGTH_SHORT).show();
                    return;
                }

                mDatabase = FirebaseDatabase.getInstance().getReference("identifikasi_kondisi");
                modelKondisi.setKondisi(etKondisi.getText().toString());
                modelKondisi.setBila_benar(arrKeyKondisi.get(spBilaBenar.getSelectedItemPosition()));
                modelKondisi.setBila_salah(arrKeyKondisi.get(spBilaSalah.getSelectedItemPosition()));
                modelKondisi.setStart(start);
                modelKondisi.setStop(stop);
                mDatabase.child(modelKondisi.getKey())
                        .setValue(modelKondisi);
                Toast.makeText(DetailKondisiActivity.this, "Data Berhasil di ubah", Toast.LENGTH_SHORT).show();
                Intent intentBackKondisi = new Intent(getApplicationContext(), ListKondisiActivity.class);
                startActivity(intentBackKondisi);
                finish();
            }
        });
    }

    private void setUpFindView() {
        etKondisi = findViewById(R.id.et_kondisi);
        spBilaBenar = findViewById(R.id.sp_benar);
        spBilaSalah = findViewById(R.id.sp_salah);
        spStart = findViewById(R.id.sp_start);
        spStop = findViewById(R.id.sp_stop);
        btnCancel = findViewById(R.id.iv_cancel_kondisi);
        btnEdit = findViewById(R.id.iv_edit_kondisi);
        btnSubmit = findViewById(R.id.iv_submit_kondisi);
        tvTitleText = findViewById(R.id.tv_title_list_kondisi);
    }

    private boolean validateForm(String kondisi) {
        boolean valid = true;

        if (TextUtils.isEmpty(kondisi)) {
            etKondisi.setError("Required.");
            valid = false;
        } else {
            etKondisi.setError(null);
        }

        return valid;
    }

    private boolean validateStart(ArrayList<ModelKondisi> data) {
        for (int i = 0; i < data.size(); i++) {
            if ("Ya".contentEquals(data.get(i).getStart())) {
                return true;
            }
        }
        return false;
    }

    private String getStartVal(ArrayList<ModelKondisi> data) {
        for (int i = 0; i < data.size(); i++) {
            if ("Ya".contentEquals(data.get(i).getStart())) {
                return data.get(i).getKey();
            }
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentBackKondisi = new Intent(getApplicationContext(), ListKondisiActivity.class);
        startActivity(intentBackKondisi);
        finish();
    }
}
