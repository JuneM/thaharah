package com.koding.thahara.ui.DetailEdit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelArtikel;
import com.koding.thahara.ui.List.ListArtikelActivity;
import com.koding.thahara.ui.List.ListCaraAtauBendaActivity;
import com.koding.thahara.ui.auth.LoginActivity;

public class DetailArtikelActivity extends AppCompatActivity {
    
    private static final String TAG_SESSION = "CekitSession";
    private ModelArtikel modelArtikel;
    public static final String EXTRA_DATA = "extra_data";
    private TextView tvTitleText;
    private EditText etJudulArtikel, etIsiArtikel;
    private ImageView btnEdit, btnCancel, btnSubmit;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_artikel);
        modelArtikel = (ModelArtikel) getIntent().getSerializableExtra(EXTRA_DATA);
        setUpFindView();
        setUi(modelArtikel);
        setFetchData(modelArtikel);
        setToolListener(modelArtikel);
    }

    private void setUi(ModelArtikel modelArtikel) {
        tvTitleText.setText("Detail Artikel");
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String uid = settings.getString("key", null);
        String role = settings.getString("role", null);
        if (role.equals("admin"))
        {
            btnEdit.setVisibility(View.VISIBLE);
        }else{
            btnEdit.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.GONE);
        }
    }

    private void setFetchData(ModelArtikel modelArtikel) {
        etJudulArtikel.setText(modelArtikel.getJudulArtikel());
        etIsiArtikel.setText(modelArtikel.getIsiArtikel());
    }

    private void setToolListener(final ModelArtikel modelArtikel) {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTitleText.setText("Detail Artikel");
                etJudulArtikel.setEnabled(false);
                etIsiArtikel.setEnabled(false);
                etJudulArtikel.setText(modelArtikel.getJudulArtikel());
                etIsiArtikel.setText(modelArtikel.getIsiArtikel());
                btnCancel.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.GONE);
                btnEdit.setVisibility(View.VISIBLE);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTitleText.setText("Edit Artikel");
                etJudulArtikel.setEnabled(true);
                etIsiArtikel.setEnabled(true);
                btnCancel.setVisibility(View.VISIBLE);
                btnEdit.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String judulArtikel = etJudulArtikel.getText().toString();
                String isiArtikel= etIsiArtikel.getText().toString();
                if (!validateForm(judulArtikel, isiArtikel)) {
                    return;
                }
                mDatabase = FirebaseDatabase.getInstance().getReference("artikel");
                modelArtikel.setJudulArtikel(etJudulArtikel.getText().toString());
                modelArtikel.setIsiArtikel(etIsiArtikel.getText().toString());
                mDatabase.child(modelArtikel.getKey())
                        .setValue(modelArtikel);
                Toast.makeText(DetailArtikelActivity.this, "Data Berhasil di ubah", Toast.LENGTH_SHORT).show();
                Intent intentBackArtikel = new Intent(getApplicationContext(), ListArtikelActivity.class);
                startActivity(intentBackArtikel);
                finish();

            }
        });
    }

    private void setUpFindView() {
        etJudulArtikel = findViewById(R.id.et_judul_arikel);
        etIsiArtikel = findViewById(R.id.et_isi_artikel);
        btnCancel = findViewById(R.id.iv_cancel_artikel);
        btnEdit = findViewById(R.id.iv_edit_artikel);
        btnSubmit = findViewById(R.id.iv_submit_artikel);
        tvTitleText = findViewById(R.id.tv_title_artikel);
    }

    private boolean validateForm(String judulArtikel, String isiArtikel) {
        boolean valid = true;

        if (TextUtils.isEmpty(judulArtikel)) {
            etJudulArtikel.setError("Required.");
            valid = false;
        } else {
            etJudulArtikel.setError(null);
        }

        if (TextUtils.isEmpty(isiArtikel)) {
            etIsiArtikel.setError("Required.");
            valid = false;
        } else {
            etIsiArtikel.setError(null);
        }
        
        return valid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentBackArtikel = new Intent(getApplicationContext(), ListArtikelActivity.class);
        startActivity(intentBackArtikel);
        finish();
    }
}
