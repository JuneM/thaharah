package com.koding.thahara.ui.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.adapter.DataUserAdapter;
import com.koding.thahara.model.ModelUser;
import com.koding.thahara.ui.auth.LoginActivity;

import java.util.ArrayList;

public class ListDataUserActivity extends BaseActivity implements DataUserAdapter.FirebaseDataListener {

    private static final String TAG_SESSION = "CekitSession";
    private DatabaseReference mDatabase;
    private ArrayList<ModelUser> modelUsersList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView tvSearch;
    private SearchView svSeacrh;

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username.isEmpty()){
            Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intentBackLogin);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datauser);
        FirebaseApp.initializeApp(this);
        setUpFindView();
        setRecycleView();
    }

    private void setUpFindView() {
        recyclerView = findViewById(R.id.rv_list_user);
        tvSearch = findViewById(R.id.tv_search);
        svSeacrh = findViewById(R.id.search);
        svSeacrh.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() != 0){
                    tvSearch.setVisibility(View.GONE);
                }else{
                    tvSearch.setVisibility(View.VISIBLE);
                }
                recyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child("users").orderByChild("username").startAt(query).endAt(query + "\uf8ff").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        modelUsersList = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelUser users = noteDataSnapshoot.getValue(ModelUser.class);
                            if (!users.getRole().equals("admin")){
                                users.setKey(noteDataSnapshoot.getKey());
                                modelUsersList.add(users);
                            }
                        }
                        adapter = new DataUserAdapter(ListDataUserActivity.this, modelUsersList);
                        recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() != 0){
                    tvSearch.setVisibility(View.GONE);
                }else{
                    tvSearch.setVisibility(View.VISIBLE);
                }
                recyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child("users").orderByChild("username").startAt(newText).endAt(newText + "\uf8ff").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        modelUsersList = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelUser users = noteDataSnapshoot.getValue(ModelUser.class);
                            if (!users.getRole().equals("admin")){
                                users.setKey(noteDataSnapshoot.getKey());
                                modelUsersList.add(users);
                            }
                        }
                        adapter = new DataUserAdapter(ListDataUserActivity.this, modelUsersList);
                        recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }
        });
    }

    private void setRecycleView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                modelUsersList = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelUser users = noteDataSnapshoot.getValue(ModelUser.class);
                    if (!users.getRole().equals("admin")){
                        users.setKey(noteDataSnapshoot.getKey());
                        modelUsersList.add(users);
                    }
                }
                adapter = new DataUserAdapter(ListDataUserActivity.this, modelUsersList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDelete(ModelUser user, int position) {
        if (mDatabase != null) {
            mDatabase.child("users").child(user.getKey()).removeValue();
        }
    }
}