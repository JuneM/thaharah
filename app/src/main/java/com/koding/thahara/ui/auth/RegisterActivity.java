package com.koding.thahara.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelUser;


public class RegisterActivity extends BaseActivity {

    private static final String TAG = "CekitUser";
    private EditText mUsernameField, mPasswordField,mRePasswordField;
    private Button btnRegister, btnCancel;
    private DatabaseReference mDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setUpView();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        toolListener();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setUpView() {
        mUsernameField = findViewById(R.id.et_username);
        mPasswordField = findViewById(R.id.et_password);
        mRePasswordField = findViewById(R.id.et_re_password);
        btnCancel = findViewById(R.id.btn_cancel);
        btnRegister = findViewById(R.id.btn_register);
    }

    private void toolListener() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCancel = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intentCancel);
                finish();
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAccount(mUsernameField.getText().toString(), mPasswordField.getText().toString(), mRePasswordField.getText().toString());
            }
        });
    }

    private void createAccount(final String username, final String password, String rePassword) {
        Log.d(TAG, "createAccount:" + username +"/n pass:"+ password);
        if (!validateForm(username, password, rePassword)) {
            return;
        }
        showProgressDialog();
        final String role = "pakar";
        final Boolean status = false;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    ModelUser newUser = new ModelUser(username,password,role,status);
                    insertDataUser(newUser);
                    hideProgressDialog();
                }else{
                    mDatabase.child("users").orderByChild("username").equalTo(username).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                hideProgressDialog();
                                Toast.makeText(getApplicationContext(), "Username Sudah Ada", Toast.LENGTH_SHORT).show();
                                mUsernameField.setFocusable(true);
                                mUsernameField.setError("data sudah ada");
                            }else{
                                hideProgressDialog();
                                ModelUser newUser = new ModelUser(username,password,role,status);
                                insertDataUser(newUser);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            hideProgressDialog();
                            Toast.makeText(RegisterActivity.this, "koneksi Error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                hideProgressDialog();
                Toast.makeText(RegisterActivity.this, "koneksi Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void insertDataUser(ModelUser newUser) {
        mDatabase.child("users")
                .push()
                .setValue(newUser)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(RegisterActivity.this, "Data Berhasil Di daftarkan", Toast.LENGTH_SHORT).show();
                    }
                });

        Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intentLogin);
        finish();
    }


    private boolean validateForm(String username, String password, String rePassword) {
        boolean valid = true;

        if (TextUtils.isEmpty(username)) {
            mUsernameField.setError("Required.");
            valid = false;
        } else {
            mUsernameField.setError(null);
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        if (TextUtils.isEmpty(rePassword)) {
            mRePasswordField.setError("Required.");
            valid = false;
        } else if (!rePassword.equals(password)) {
            mRePasswordField.setError("RePassword Harus Sama.");
            valid = false;
        } else {
            mRePasswordField.setError(null);
        }
        return valid;
    }
}
