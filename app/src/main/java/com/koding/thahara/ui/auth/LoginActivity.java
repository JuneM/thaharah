package com.koding.thahara.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thahara.BaseActivity;
import com.koding.thahara.R;
import com.koding.thahara.model.ModelUser;
import com.koding.thahara.ui.HomeActivity;

import java.util.ArrayList;

public class LoginActivity extends BaseActivity {
    private static final String TAG = "CekitUser";
    private static final String TAG_SESSION = "CekitSession";
    private ArrayList<ModelUser> modelUsersList;
    private DatabaseReference mDatabase;
    private EditText mUsernameField, mPasswordField;
    private Button btnSubmit, btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        setUpView();
        toolListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences settings;
        settings = getApplication().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
        String username = settings.getString("username", null);
        if (username != null){
            if (username.isEmpty()){
                Intent intentBackLogin = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intentBackLogin);
                finish();
            }else{
                Intent intentHome = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intentHome);
                finish();
            }
        }
    }

    private void setUpView() {
        mUsernameField = findViewById(R.id.et_username);
        mPasswordField = findViewById(R.id.et_password);
        btnSubmit = findViewById(R.id.btn_login);
        btnSignUp = findViewById(R.id.btn_signup);
    }

    private void toolListener() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authUser(mUsernameField.getText().toString(), mPasswordField.getText().toString());
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSignUp = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intentSignUp);
                finish();
            }
        });
    }

    private void authUser(final String username, final String password) {
        Log.d(TAG, "createAccount:" + username +"/n pass:"+ password);
        if (!validateForm(username, password)) {
            return;
        }
        showProgressDialog();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    mDatabase.child("users").orderByChild("username").equalTo(username).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            modelUsersList = new ArrayList<>();
                            for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                                ModelUser users = noteDataSnapshoot.getValue(ModelUser.class);
                                users.setKey(noteDataSnapshoot.getKey());
                                modelUsersList.add(users);
                            }

                           if (modelUsersList.size() != 0){
                               ModelUser user = modelUsersList.get(0);
                               if(user.pass.equals(password)){
                                   SharedPreferences shareSession = getApplicationContext().getSharedPreferences(TAG_SESSION, Context.MODE_PRIVATE);
                                   SharedPreferences.Editor editor = shareSession.edit();
                                   editor.putString("key",user.key);
                                   editor.putString("username",user.username);
                                   editor.putString("password", user.pass);
                                   editor.putString("role", user.role);
                                   editor.putString("status", user.status.toString());
                                   editor.commit();
                                   Intent intentLogin = new Intent(getApplicationContext(), HomeActivity.class);
                                   startActivity(intentLogin);
                                   finish();
                                   hideProgressDialog();
                               }else{
                                   mUsernameField.setFocusable(true);
                                   mUsernameField.setError("username dan password salah");
                                   Toast.makeText(LoginActivity.this, "username dan password salah", Toast.LENGTH_SHORT).show();
                                   hideProgressDialog();
                               }
                           }else{
                               hideProgressDialog();
                               Toast.makeText(LoginActivity.this, "Data Tidak Ada", Toast.LENGTH_SHORT).show();
                           }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(LoginActivity.this, "koneksi Error", Toast.LENGTH_SHORT).show();
                            hideProgressDialog();
                        }
                    });
                }else{
                    mUsernameField.setFocusable(true);
                    mUsernameField.setError("data sudah ada");
                    Toast.makeText(LoginActivity.this, "Anda Belum Daftar", Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                hideProgressDialog();
                Toast.makeText(LoginActivity.this, "koneksi Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validateForm(String username, String password) {
        boolean valid = true;

        if (TextUtils.isEmpty(username)) {
            mUsernameField.setError("Required.");
            valid = false;
        } else {
            mUsernameField.setError(null);
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }
}
